package com.atww.back.exception;

import com.atww.back.domain.vo.ResultVo;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/4/11 15:39
 * @bug退散
 */
@RestControllerAdvice
public class AnyException {
    @ExceptionHandler(RuntimeException.class)
    public ResultVo anyError(RuntimeException e){
        return ResultVo.fail(e.getMessage());
    }
}
