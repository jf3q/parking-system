package com.atww.back.utils;

import io.jsonwebtoken.*;

import java.util.Date;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/3/25 11:25
 * @bug退散
 */
public class JwtUtils {
    public static String getToken(String username){
        return Jwts.builder()
                .setHeaderParam("typ","jwt")
                .setExpiration(new Date(System.currentTimeMillis()+360000))
                .setSubject(username)
                .signWith(SignatureAlgorithm.HS256,"jwt-ww")
                .compact();
    }
    public static String getUserName(String token){
        try {
           return Jwts.parser().setSigningKey("jwt-ww").parseClaimsJws(token).getBody().getSubject();
        } catch (ExpiredJwtException e) {
            throw new RuntimeException("token过期啦!");
        } catch (UnsupportedJwtException e) {
            throw new RuntimeException("不支持的token!");
        } catch (MalformedJwtException e) {
            throw new RuntimeException("token格式有误!");
        } catch (SignatureException e) {
            throw new RuntimeException("密钥签名有误!");
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("不合法的参数!");
        }
    }
}
