package com.atww.back.controller;

import com.atww.back.domain.dto.UserDto;
import com.atww.back.domain.pojo.User;
import com.atww.back.domain.vo.ResultVo;
import com.atww.back.service.UserService;
import com.atww.back.utils.JwtUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/4/25 16:01
 * @bug退散
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping
    public ResultVo saveOrUpdate(@RequestBody User user){
        userService.saveOrUpdate(user);
        return ResultVo.success();
    }

    @DeleteMapping("/{id}")
    public ResultVo del(@PathVariable Integer id){
        userService.removeById(id);
        return ResultVo.success();
    }

    //校验账号唯一性
    @PostMapping("/validate")
    public ResultVo validate(@RequestBody User user){

        if (StringUtils.hasText(user.getUsername())) {

            LambdaQueryWrapper<User> queryWrapper= new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getUsername,user.getUsername());
            long count = userService.count(queryWrapper);
            if (count>0){
                if(user.getId()!=null){
                    User realUser = userService.getById(user.getId());
                    if(realUser.getUsername().equals(user.getUsername())){
                        return ResultVo.success();
                    }
                    return ResultVo.fail("账号已存在");
                }
                return ResultVo.fail("账号已存在");
            }else{
                return ResultVo.success();
            }
        }else{
            return ResultVo.fail("账号必填");
        }

    }

    //注册
    @PostMapping("/reg")
    public ResultVo reg(@RequestBody User user){


        //能注册的就是普通用户
        user.setType(1);
        userService.save(user);
        return ResultVo.success();


    }

    @PostMapping("/login")
    public ResultVo login(@RequestBody UserDto userDto) {
        if (ObjectUtils.isEmpty(userDto)) throw new RuntimeException("参数不能为空!");
        User user = userService.getOne(new LambdaQueryWrapper<User>()
                .eq(StringUtils.hasText(userDto.getUsername()), User::getUsername, userDto.getUsername())
                .eq(StringUtils.hasText(userDto.getPassword()), User::getPassword, userDto.getPassword()));
        if (user == null) throw new RuntimeException("用户名或密码错误!");

        //登录成功 返回token和用户类别
        String token = JwtUtils.getToken(user.getUsername());
        Map<String, Object> map = new HashMap<>();
        map.put("token", token);
        map.put("userType", user.getType());
        return ResultVo.success("登录成功!", map);
    }
    @GetMapping("/info")
    public ResultVo getInfo(HttpServletRequest request) {
        String token = request.getHeader("token");
        String userName = JwtUtils.getUserName(token);
        Map<String, Object> map = new HashMap<>();
        map.put("name",userName);
        map.put("avatar","https://profile-avatar.csdnimg.cn/f56471461f4e4a81937aa740d065ee59_m0_74994499.jpg!1");
        return ResultVo.success("", map);
    }

    @PostMapping("/logout")
    public ResultVo logout(){
        return ResultVo.success();
    }

    @PostMapping("/page")
    public ResultVo getPage(@RequestBody(required = false) User user,
                            @RequestParam(defaultValue = "1") Integer pageNum,
                            @RequestParam(defaultValue = ""+ Integer.MAX_VALUE) Integer pageSize) {

        LambdaQueryWrapper<User> queryWrapper= new LambdaQueryWrapper<>();

        if(user!=null){
            if (StringUtils.hasText(user.getName())) {
                queryWrapper.like(User::getName,user.getName());
            }
            if (!ObjectUtils.isEmpty(user.getType())) {
                queryWrapper.like(User::getType,user.getType());
            }
        }

        queryWrapper.orderByDesc(User::getId);
        Page<User> page = userService.page(new Page<>(pageNum, pageSize), queryWrapper);
        return ResultVo.success("",page);

    }

    @GetMapping("/showOne/{username}")
    public ResultVo showOne(@PathVariable String username){
        LambdaQueryWrapper<User> lambdaQueryWrapper=new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(User::getUsername,username);
        User user = userService.getOne(lambdaQueryWrapper);
        return ResultVo.success("",user);
    }


}
