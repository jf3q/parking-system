package com.atww.back.controller;

import com.atww.back.domain.dto.ParkingSpaceDto;
import com.atww.back.domain.pojo.ParkingLot;
import com.atww.back.domain.pojo.ParkingSpace;
import com.atww.back.domain.pojo.User;
import com.atww.back.domain.vo.ResultVo;
import com.atww.back.service.ParkingLotService;
import com.atww.back.service.ParkingSpaceService;
import com.atww.back.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/parkingSpace")
public class ParkingSpaceController {

    @Autowired
    ParkingSpaceService spaceService;

    @Autowired
    ParkingLotService lotService;
    @Autowired
    UserService userService;


    @DeleteMapping("/del/{id}")
    public ResultVo del(@PathVariable Integer id){
        spaceService.removeById(id);
        return ResultVo.success();
    }



    @PostMapping("/getPage/{pageCount}/{pId}")
    public ResultVo getPage(@PathVariable Integer pageCount, @PathVariable Integer pId,@RequestBody ParkingSpaceDto spaceDto){
        Page page=spaceService.getPage(spaceDto,pageCount,pId);
        return ResultVo.success("",page);
    }

    @PostMapping("/saveOrEdit")
    public ResultVo saveOrEdit(@RequestBody ParkingSpace parkingSpace){
        spaceService.saveOrUpdate(parkingSpace);
        return ResultVo.success();
    }

    @GetMapping("/getUsers")
    public ResultVo getUsers(){
        LambdaQueryWrapper<User> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getType,2);
        List<User> list = userService.list(queryWrapper);
        return ResultVo.success("",list);
    }


}
