package com.atww.back.controller;

import com.atww.back.domain.dto.ParkingLotDto;
import com.atww.back.domain.pojo.ParkingLot;
import com.atww.back.domain.pojo.User;
import com.atww.back.domain.vo.ResultVo;
import com.atww.back.service.ParkingLotService;
import com.atww.back.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * 停车场
 * @author 汪汪
 * @version 1.0
 * @data 2024/4/26 11:47
 * @bug退散
 */
@RestController
@RequestMapping("/parkingLot")
public class ParkingLotController {
    @Autowired
    ParkingLotService parkingLotService;
    @Autowired
    UserService userService;

    @PostMapping
    public ResultVo saveOrUpdate(@RequestBody ParkingLot parkingLot){
        parkingLotService.saveOrUpdate(parkingLot);
        return ResultVo.success();
    }


    //查最近的停车场
    @GetMapping("/{lng}/{lat}")
    public ResultVo getNearLot(@PathVariable BigDecimal lng, @PathVariable BigDecimal lat){
        ParkingLot lot=parkingLotService.getNearLot(lng,lat);
        return ResultVo.success("获取附近的停车场",lot);

    }

    //校验停车厂唯一性
    @PostMapping("/validate")
    public ResultVo validate(@RequestBody ParkingLot parkingLot){

        if (StringUtils.hasText(parkingLot.getName())) {
            LambdaQueryWrapper<ParkingLot> queryWrapper= new LambdaQueryWrapper<>();
            queryWrapper.eq(ParkingLot::getName,parkingLot.getName());
            long count = parkingLotService.count(queryWrapper);
            if (count>0){
                if(parkingLot.getId()!=null){
                    ParkingLot byId = parkingLotService.getById(parkingLot.getId());
                    if(byId.getName().equals(parkingLot.getName())){
                        return ResultVo.success();
                    }
                }
                return ResultVo.fail("该停车场已存在");
            }else{
                return ResultVo.success();
            }
        }else{
            return ResultVo.fail("停车场名称必填");
        }


    }

    @DeleteMapping("/{id}")
    public ResultVo del(@PathVariable Integer id){
        parkingLotService.removeById(id);
        return ResultVo.success();
    }


    @PostMapping("/page/{pageCount}/{uname}")
    public ResultVo getPage(@PathVariable Integer pageCount,@PathVariable String uname, @RequestBody ParkingLotDto parkingLotDto){
        LambdaQueryWrapper<User> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getUsername,uname);
        User user = userService.getOne(queryWrapper);
        parkingLotDto.setUserId(user.getId());
        if(user.getType()==0||user.getType()==1){
            parkingLotDto.setUserId(0);
        }
        Page page=parkingLotService.getPage(pageCount,parkingLotDto);
        return ResultVo.success("",page);
    }

    @GetMapping("/getLotUser")
    public ResultVo getLotUser(){
        LambdaQueryWrapper<User> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getType,2);
        List<User> list = userService.list(queryWrapper);
        return ResultVo.success("",list);
    }

}
