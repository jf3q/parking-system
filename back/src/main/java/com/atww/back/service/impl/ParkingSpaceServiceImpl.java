package com.atww.back.service.impl;

import com.atww.back.domain.dto.ParkingSpaceDto;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atww.back.domain.pojo.ParkingSpace;
import com.atww.back.service.ParkingSpaceService;
import com.atww.back.mapper.ParkingSpaceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【t_parking_space】的数据库操作Service实现
* @createDate 2024-04-25 16:00:09
*/
@Service
public class ParkingSpaceServiceImpl extends ServiceImpl<ParkingSpaceMapper, ParkingSpace>
    implements ParkingSpaceService{
    @Autowired
    ParkingSpaceMapper parkingSpaceMapper;

    @Override
    public Page getPage(ParkingSpaceDto spaceDto, Integer pageCount,Integer pId) {
        return parkingSpaceMapper.getPage(new Page<ParkingSpace>(spaceDto.getPageNow(),pageCount),pId,spaceDto.getStatus());
    }
}




