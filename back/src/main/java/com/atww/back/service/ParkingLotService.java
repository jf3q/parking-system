package com.atww.back.service;

import com.atww.back.domain.dto.ParkingLotDto;
import com.atww.back.domain.pojo.ParkingLot;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;

/**
* @author Administrator
* @description 针对表【t_parking_lot】的数据库操作Service
* @createDate 2024-04-26 11:46:20
*/
public interface ParkingLotService extends IService<ParkingLot> {

    ParkingLot getNearLot(BigDecimal lng, BigDecimal lat);

    Page getPage(Integer pageCount, ParkingLotDto parkingLotDto);
}
