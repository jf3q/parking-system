package com.atww.back.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atww.back.domain.pojo.User;
import com.atww.back.service.UserService;
import com.atww.back.mapper.UserMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【t_user】的数据库操作Service实现
* @createDate 2024-04-25 16:00:09
*/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
    implements UserService{

}




