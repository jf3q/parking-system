package com.atww.back.service;

import com.atww.back.domain.dto.OrderPageDto;
import com.atww.back.domain.pojo.Order;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【t_order】的数据库操作Service
* @createDate 2024-04-25 16:00:09
*/
public interface OrderService extends IService<Order> {

    Page getPage(Integer pageCount, OrderPageDto pageDto);
}
