package com.atww.back.service;

import com.atww.back.domain.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【t_user】的数据库操作Service
* @createDate 2024-04-25 16:00:09
*/
public interface UserService extends IService<User> {

}
