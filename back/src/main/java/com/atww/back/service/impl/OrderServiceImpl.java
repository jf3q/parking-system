package com.atww.back.service.impl;

import com.atww.back.domain.dto.OrderPageDto;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atww.back.domain.pojo.Order;
import com.atww.back.service.OrderService;
import com.atww.back.mapper.OrderMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【t_order】的数据库操作Service实现
* @createDate 2024-04-25 16:00:09
*/
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order>
    implements OrderService{

    @Autowired
    OrderMapper orderMapper;

    @Override
    public Page getPage(Integer pageCount, OrderPageDto pageDto) {
        return orderMapper.getPage(new Page<Order>(pageDto.getPageNow(),pageCount),pageDto.getParkingLotName(),pageDto.getParkingSpaceName(),pageDto.getUserId(),pageDto.getPrincipalId());
    }
}




