package com.atww.back.service;

import com.atww.back.domain.dto.ParkingSpaceDto;
import com.atww.back.domain.pojo.ParkingSpace;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【t_parking_space】的数据库操作Service
* @createDate 2024-04-25 16:00:09
*/
public interface ParkingSpaceService extends IService<ParkingSpace> {

    Page getPage(ParkingSpaceDto spaceDto, Integer pageCount,Integer pId);
}
