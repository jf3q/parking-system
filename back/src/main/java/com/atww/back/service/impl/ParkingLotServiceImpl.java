package com.atww.back.service.impl;

import com.atww.back.domain.dto.ParkingLotDto;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atww.back.domain.pojo.ParkingLot;
import com.atww.back.service.ParkingLotService;
import com.atww.back.mapper.ParkingLotMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
* @author Administrator
* @description 针对表【t_parking_lot】的数据库操作Service实现
* @createDate 2024-04-26 11:46:20
*/
@Service
public class ParkingLotServiceImpl extends ServiceImpl<ParkingLotMapper, ParkingLot>
    implements ParkingLotService{

    @Autowired
    ParkingLotMapper lotMapper;

    @Override
    public ParkingLot getNearLot(BigDecimal lng, BigDecimal lat) {

        return lotMapper.getNearLot(lng,lat);
    }

    @Override
    public Page getPage(Integer pageCount, ParkingLotDto parkingLotDto) {
        return lotMapper.getPage(new Page<>(parkingLotDto.getPageNow(),pageCount),parkingLotDto.getName(),parkingLotDto.getUserId());
    }
}




