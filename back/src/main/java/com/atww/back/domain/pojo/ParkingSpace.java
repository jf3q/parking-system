package com.atww.back.domain.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 *
 * @TableName t_parking_space
 */
@TableName(value ="t_parking_space")
@Data
public class ParkingSpace implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 停车场id
     */
    private Integer parkingLotId;

    @TableField(exist = false)
    private String parkingLotName;

    /**
     * 停车场车位
     */
    private String name;

    /**
     * 车位状态: 0:闲置 1:有人
     */
    private Integer status;


    private Integer userId;//负责人id
    @TableField(exist = false)
    private String userName;//负责人姓名

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
