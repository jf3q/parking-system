package com.atww.back.domain.dto;

import lombok.Data;

/**
 * @author : 八九宸烨
 * @date :
 * 正在播放《onion boy》
 * ●━━━━━━─────── 1:27
 * ⇆ ◁ ❚❚ ▷ ↻
 */
@Data
public class OrderPageDto {
    private Integer pageNow;
    private String parkingSpaceName;
    private String parkingLotName;
    private Integer userId;
    private Integer principalId;
    private String userName;
}
