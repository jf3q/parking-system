package com.atww.back.domain.dto;

import lombok.Data;

/**
 * @author : 八九宸烨
 * @date :
 * 正在播放《》
 * ●━━━━━━─────── 1:27
 * ⇆ ◁ ❚❚ ▷ ↻
 */
@Data
public class ParkingLotDto {
    private Integer pageNow;
    private String name;
    private Integer userId;
}
