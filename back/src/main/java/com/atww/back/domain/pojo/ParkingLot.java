package com.atww.back.domain.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;

/**
 *
 * @TableName t_parking_lot
 */
@TableName(value ="t_parking_lot")
@Data
public class ParkingLot implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 停车场名字
     */
    private String name;

    /**
     * 停车场纬度
     */
    private BigDecimal latitude;

    /**
     * 停车场经度
     */
    private BigDecimal longitude;

    private Integer userId;//负责人id
    @TableField(exist = false)
    private String userName;//负责人姓名

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
