package com.atww.back.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : 八九宸烨
 * @date :
 * 正在播放《》
 * ●━━━━━━─────── 1:27
 * ⇆ ◁ ❚❚ ▷ ↻
 */
@Data
public class ParkingSpaceDto {
    private Integer pageNow;
    private Integer status;
}
