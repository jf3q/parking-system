package com.atww.back.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/4/25 16:06
 * @bug退散
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultVo {
    private Integer code;
    private String message;
    private Object data;

    public static ResultVo success( String message,Object data) {
        return new ResultVo(20000,message , data);
    }
    public static ResultVo success( ) {
        return new ResultVo(20000,"" , null);
    }

    public static ResultVo fail( String message) {
        return new ResultVo(50000, message, null);
    }
}
