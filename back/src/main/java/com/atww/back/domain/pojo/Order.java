package com.atww.back.domain.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @TableName t_order
 */
@TableName(value ="t_order")
@Data
public class Order implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer userId;
    @TableField(exist = false)
    private String userName;

    private Integer principalId;

    /**
     * 停车场id
     */
    private Integer parkingLotId;

    @TableField(exist = false)
    private String parkingLotName;

    /**
     * 车位id
     */
    private Integer parkingSpaceId;

    @TableField(exist = false)
    private String parkingSpaceName;

    /**
     * 价格
     */
    private BigDecimal price;


    private Date arrivalTime;

    private Date departureTime;

    private Integer status;

    /**
     * 创建时间
     */
    @TableField(value = "createTime",fill = FieldFill.INSERT)
    private Date createtime;

    /**
     * 修改时间
     */
    @TableField(value = "updateTime",fill = FieldFill.INSERT_UPDATE)
    private Date updatetime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
