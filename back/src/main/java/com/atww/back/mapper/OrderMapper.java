package com.atww.back.mapper;

import com.atww.back.domain.pojo.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
* @author Administrator
* @description 针对表【t_order】的数据库操作Mapper
* @createDate 2024-04-25 16:00:09
* @Entity com.atww.back.domain.pojo.Order
*/
public interface OrderMapper extends BaseMapper<Order> {


    Page getPage(Page<Order> orderPage, String parkingLotName, String parkingSpaceName, Integer userId, Integer principalId);
}




