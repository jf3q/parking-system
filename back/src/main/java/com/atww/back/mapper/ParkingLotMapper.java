package com.atww.back.mapper;

import com.atww.back.domain.pojo.ParkingLot;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
* @author Administrator
* @description 针对表【t_parking_lot】的数据库操作Mapper
* @createDate 2024-04-26 11:46:20
* @Entity com.atww.back.domain.pojo.ParkingLot
*/
public interface ParkingLotMapper extends BaseMapper<ParkingLot> {

    ParkingLot getNearLot(@Param("lng") BigDecimal lng, @Param("lat")BigDecimal lat);


    Page getPage(Page<Object> objectPage, String name, Integer userId);
}




