package com.atww.back.mapper;

import com.atww.back.domain.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【t_user】的数据库操作Mapper
* @createDate 2024-04-25 16:00:09
* @Entity com.atww.back.domain.pojo.User
*/
public interface UserMapper extends BaseMapper<User> {

}




