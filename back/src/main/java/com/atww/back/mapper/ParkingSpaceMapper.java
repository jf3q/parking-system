package com.atww.back.mapper;

import com.atww.back.domain.dto.ParkingSpaceDto;
import com.atww.back.domain.pojo.ParkingSpace;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
* @author Administrator
* @description 针对表【t_parking_space】的数据库操作Mapper
* @createDate 2024-04-25 16:00:09
* @Entity com.atww.back.domain.pojo.ParkingSpace
*/
public interface ParkingSpaceMapper extends BaseMapper<ParkingSpace> {


    Page getPage(Page<ParkingSpace> parkingSpacePage, Integer pId, Integer status);
}




