module.exports = {
  devServer: {
    port: 8888,
    proxy: {
      [process.env.VUE_APP_BASE_API]: {
        target: 'http://localhost:80',
        changeOrigin: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: ''
        }
      }
    }
  }
}
