import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'
import Layout from '@/layout/index'// 引入Layout

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login'] // no redirect whitelist

router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start()

  // set page title
  document.title = getPageTitle(to.meta.title)

  // determine whether the user has logged in
  const hasToken = getToken()

  if (hasToken) {
    if (to.path === '/login') {
      // if is logged in, redirect to the home page
      next({ path: '/' })
      NProgress.done()
    } else {
      const hasGetUserInfo = store.getters.name
      if (hasGetUserInfo) {
        next()
      } else {
        try {
          // get user info
          await store.dispatch('user/getInfo')
          let menus =[]
          if(store.getters.role =='admin'){
            menus =[ {
              path: '/map',
              component: Layout,
              redirect: '/map',
              children: [{
                path: 'map',
                name: 'Map',
                component: () => import('@/views/map/index.vue'),
                meta: { title: '地图', icon: 'dashboard' }
              }]
            },{
              path: '/user',
              component: Layout,
              redirect: '/user',
              children: [{
                path: 'user',
                name: 'user',
                component: () => import('@/views/user/index.vue'),
                meta: { title: '用户管理', icon: 'dashboard' }
              }]
            },
              {
              path: '/lot',
              component: Layout,
              redirect: '/lot',
              children: [{
                path: 'lot',
                name: 'lot',
                component: () => import('@/views/lot/index.vue'),
                meta: { title: '停车场管理', icon: 'dashboard' }
              },
              ]
            },
              {
                path: '/car',
                component: Layout,
                redirect: '/car',
                hidden:true,
                children: [{
                  path: 'car',
                  name: 'car',
                  component: () => import('@/views/car/index.vue'),
                  meta: { title: '停车位管理', icon: 'dashboard' }
                },
                ]
              },
              {
                path: '/editpass',
                component: Layout,
                redirect: '/editpass',
                hidden:true,
                children: [{
                  path: 'editpass',
                  name: 'editpass',
                  component: () => import('@/views/editpass/index.vue'),
                  meta: { title: '个人中心', icon: 'dashboard' }
                },
                ]
              },
              {
                path: '/addorder',
                component: Layout,
                redirect: '/addorder',
                hidden:true,
                children: [{
                  path: 'addorder',
                  name: 'addorder',
                  component: () => import('@/views/addorder/index.vue'),
                  meta: { title: '预定车位信息', icon: 'dashboard' }
                },
                ]
              },
              {
                path: '/order',
                component: Layout,
                redirect: '/order',
                children: [{
                  path: 'order',
                  name: 'order',
                  component: () => import('@/views/order/index.vue'),
                  meta: { title: '订单信息', icon: 'dashboard' }
                },
                ]
              },
            ]
          }
          if(store.getters.role =='lot_admin'){
            menus =[ {
              path: '/map',
              component: Layout,
              redirect: '/map',
              children: [{
                path: 'map',
                name: 'Map',
                component: () => import('@/views/map/index.vue'),
                meta: { title: '地图', icon: 'dashboard' }
              }]
            },
              {
                path: '/lot',
                component: Layout,
                redirect: '/lot',
                children: [{
                  path: 'lot',
                  name: 'lot',
                  component: () => import('@/views/lot/index.vue'),
                  meta: { title: '停车场管理', icon: 'dashboard' }
                },
                ]
              },
              {
                path: '/car',
                component: Layout,
                redirect: '/car',
                hidden:true,
                children: [{
                  path: 'car',
                  name: 'car',
                  component: () => import('@/views/car/index.vue'),
                  meta: { title: '停车位管理', icon: 'dashboard' }
                },
                ]
              },
              {
                path: '/editpass',
                component: Layout,
                redirect: '/editpass',
                hidden:true,
                children: [{
                  path: 'editpass',
                  name: 'editpass',
                  component: () => import('@/views/editpass/index.vue'),
                  meta: { title: '个人中心', icon: 'dashboard' }
                },
                ]
              },
              {
                path: '/addorder',
                component: Layout,
                redirect: '/addorder',
                hidden:true,
                children: [{
                  path: 'addorder',
                  name: 'addorder',
                  component: () => import('@/views/addorder/index.vue'),
                  meta: { title: '预定车位信息', icon: 'dashboard' }
                },
                ]
              },
              {
                path: '/order',
                component: Layout,
                redirect: '/order',
                children: [{
                  path: 'order',
                  name: 'order',
                  component: () => import('@/views/order/index.vue'),
                  meta: { title: '订单信息', icon: 'dashboard' }
                },
                ]
              },
            ]
          }
          if(store.getters.role =='pt'){
            menus =[ {
              path: '/map',
              component: Layout,
              redirect: '/map',
              children: [{
                path: 'map',
                name: 'Map',
                component: () => import('@/views/map/index.vue'),
                meta: { title: '地图', icon: 'dashboard' }
              }]
            },
              {
                path: '/lot',
                component: Layout,
                redirect: '/lot',
                children: [{
                  path: 'lot',
                  name: 'lot',
                  component: () => import('@/views/lot/index.vue'),
                  meta: { title: '停车场信息', icon: 'dashboard' }
                },
                ]
              },
              {
                path: '/car',
                component: Layout,
                redirect: '/car',
                hidden:true,
                children: [{
                  path: 'car',
                  name: 'car',
                  component: () => import('@/views/car/index.vue'),
                  meta: { title: '停车位信息', icon: 'dashboard' }
                },
                ]
              },
              {
                path: '/editpass',
                component: Layout,
                redirect: '/editpass',
                hidden:true,
                children: [{
                  path: 'editpass',
                  name: 'editpass',
                  component: () => import('@/views/editpass/index.vue'),
                  meta: { title: '个人中心', icon: 'dashboard' }
                },
                ]
              },
              {
                path: '/addorder',
                component: Layout,
                redirect: '/addorder',
                hidden:true,
                children: [{
                  path: 'addorder',
                  name: 'addorder',
                  component: () => import('@/views/addorder/index.vue'),
                  meta: { title: '预定车位信息', icon: 'dashboard' }
                },
                ]
              },
              {
                path: '/order',
                component: Layout,
                redirect: '/order',
                children: [{
                  path: 'order',
                  name: 'order',
                  component: () => import('@/views/order/index.vue'),
                  meta: { title: '订单信息', icon: 'dashboard' }
                },
                ]
              },
            ]
          }

          router.addRoutes(menus) // 动态添加路由
          global.antRouter = menus // 将路由数据传递给全局变量，做侧边栏菜单渲染工作
          next({ ...to, replace: true })
        } catch (error) {
          // remove token and go to login page to re-login
          await store.dispatch('user/resetToken')
          Message.error(error || 'Has Error')
          next(`/login?redirect=${to.path}`)
          NProgress.done()
        }
      }
    }
  } else {
    /* has no token*/

    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next()
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
