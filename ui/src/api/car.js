import request from "@/utils/request";

export function getPage(data,count,pId) {
  return request({
    url: '/parkingSpace/getPage/'+count+"/"+pId,
    method: 'post',
    data:data
  })
}
export function addOrEdit(data) {
  return request({
    url: '/parkingSpace/saveOrEdit',
    method: 'post',
    data:data
  })
}
export function del(data) {
  return request({
    url: '/parkingSpace/del/'+data,
    method: 'delete',
  })
}
export function getUsers() {
  return request({
    url: '/parkingSpace/getUsers',
    method: 'get',
  })
}
