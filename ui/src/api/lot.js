import request from '@/utils/request'


export function validate(data) {
  return request({
    url: '/parkingLot/validate',
    method: 'post',
    data:data
  })
}

export function getLotPage(data,pageSize,uname) {
  return request({
    url: '/parkingLot/page/'+pageSize+"/"+uname,
    method: 'post',
    data: data
  })
}
export function del(id) {
  return request({
    url: '/parkingLot/'+id,
    method: 'delete'
  })
}

export function saveOrUpdate(data) {
  return request({
    url: '/parkingLot',
    method: 'post',
    data:data
  })
}

export function getLotUser() {
  return request({
    url: '/parkingLot/getLotUser',
    method: 'get',
  })
}


export function getLnt(lng,lat) {
  return request({
    url: '/parkingLot/'+lng+"/"+lat,
    method: 'get',
  })
}

