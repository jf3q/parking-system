import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data:data
  })
}

export function getInfo() {
  return request({
    url: '/user/info',
    method: 'get',
  })
}
export function validate(value) {
  return request({
    url: '/user/validate',
    method: 'post',
    data: value
  })
}

export function getPage(data,pageNum,pageSize) {
  return request({
    url: '/user/page?pageNum='+pageNum+'&pageSize='+pageSize,
    method: 'post',
    data: data
  })
}
export function del(id) {
  return request({
    url: '/user/'+id,
    method: 'delete'
  })
}

export function saveOrUpdate(data) {
  return request({
    url: '/user',
    method: 'post',
    data:data
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}

export function showOne(data) {
  return request({
    url: '/user/showOne/'+data,
    method: 'get'
  })
}
