import request from "@/utils/request";

export function getSpaceList(data) {
  return request({
    url: '/order/getSpace/'+data,
    method: 'get',
  })
}
export function addOrupdate(data) {
  return request({
    url: '/order',
    method: 'post',
    data:data
  })
}

export function getPage(data,pageCount) {
  return request({
    url: '/order/getPage/'+pageCount,
    method: 'post',
    data:data
  })
}

export function delOrder(data) {
  return request({
    url: '/order/'+data,
    method: 'delete',
  })
}

export function change(id,psId,uname) {
  return request({
    url: '/order/change/'+id+"/"+psId+"/"+uname,
    method: 'post',
  })
}
