/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 80022
Source Host           : localhost:3306
Source Database       : parking_management

Target Server Type    : MYSQL
Target Server Version : 80022
File Encoding         : 65001

Date: 2024-05-06 22:30:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_order
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order` (
  `id` int NOT NULL AUTO_INCREMENT,
  `parking_lot_id` int NOT NULL COMMENT '车库id',
  `user_id` int DEFAULT NULL,
  `parking_space_id` int NOT NULL COMMENT '车位id',
  `price` decimal(10,2) NOT NULL COMMENT '价格',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `updateTime` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_order
-- ----------------------------

-- ----------------------------
-- Table structure for t_parking_lot
-- ----------------------------
DROP TABLE IF EXISTS `t_parking_lot`;
CREATE TABLE `t_parking_lot` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '停车场名字',
  `latitude` decimal(10,7) NOT NULL COMMENT '停车场纬度',
  `longitude` decimal(10,7) NOT NULL COMMENT '停车场经度',
  `user_id` int DEFAULT NULL COMMENT '停车场管理员',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_parking_lot
-- ----------------------------
INSERT INTO `t_parking_lot` VALUES ('1', '北大青鸟校内停车场', '29.2500000', '113.0900000', '3');
INSERT INTO `t_parking_lot` VALUES ('2', '岳阳火车站', '29.3800000', '113.1200000', '3');
INSERT INTO `t_parking_lot` VALUES ('3', '岳阳步步高', '29.3600000', '113.1400000', '3');

-- ----------------------------
-- Table structure for t_parking_space
-- ----------------------------
DROP TABLE IF EXISTS `t_parking_space`;
CREATE TABLE `t_parking_space` (
  `id` int NOT NULL AUTO_INCREMENT,
  `parking_lot_id` int NOT NULL COMMENT '停车场id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '停车场车位',
  `status` tinyint NOT NULL COMMENT '车位状态: 0:闲置 1:有人',
  `user_id` int DEFAULT NULL COMMENT '停车场管理员id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_parking_space
-- ----------------------------

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '登录名',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户密码',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户姓名',
  `type` tinyint NOT NULL COMMENT '用户类型 0:管理员 1:用户2车库管理员',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'admin', '123456', '张三', '0');
INSERT INTO `t_user` VALUES ('2', 'test', '123456', '李四', '1');
INSERT INTO `t_user` VALUES ('3', 'wangwu1', '123456', '王五', '2');
